/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.ability;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.utils.PacMap;

import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.sample.ResourceTable;

import java.util.ArrayList;
import java.util.List;

/**
 * 旋转页面
 *
 * @since 2021-07-23
 */
public class RotationAbility extends Ability {
    private static final String KEY_IS_DIALOG_SHOWN = "IS_DIALOG_SHOWN";
    private static final String KEY_CURRENT_POSITION = "CURRENT_POSITION";
    private boolean isDialogShown = false;
    private int currentPosition = 0;
    private StfalconImageViewer<Integer> viewer;
    private Image rotationDemoImage;
    private long lastClick = 0;
    private final List<Integer> posters = new ArrayList<>();

    {
        posters.add(ResourceTable.Media_vincent);
        posters.add(ResourceTable.Media_jules);
        posters.add(ResourceTable.Media_korben);
        posters.add(ResourceTable.Media_toretto);
        posters.add(ResourceTable.Media_marty);
        posters.add(ResourceTable.Media_driver);
        posters.add(ResourceTable.Media_frank);
        posters.add(ResourceTable.Media_max);
        posters.add(ResourceTable.Media_daniel);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_rotation);
        rotationDemoImage = (Image) findComponentById(ResourceTable.Id_rotationDemoImage);
        rotationDemoImage.setClickedListener(component ->
        {
            long now = System.currentTimeMillis();
            if (lastClick == 0 || now - lastClick > 1000) {
                openViewer(0);
            }
            lastClick = now;
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (viewer != null) {
            viewer.dismiss();
        }
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        outState.putBooleanValue(KEY_IS_DIALOG_SHOWN, isDialogShown);
        outState.putIntValue(KEY_CURRENT_POSITION, currentPosition);
        super.onSaveAbilityState(outState);
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        if (inState != null) {
            isDialogShown = inState.getBooleanValue(KEY_IS_DIALOG_SHOWN);
            currentPosition = inState.getIntValue(KEY_CURRENT_POSITION);
        }
        if (isDialogShown) {
            rotationDemoImage.setBindStateChangedListener(new Component.BindStateChangedListener() {
                @Override
                public void onComponentBoundToWindow(Component component) {
                    if (isDialogShown) {
                        openViewer(currentPosition);
                    }
                }

                @Override
                public void onComponentUnboundFromWindow(Component component) {
                }
            });
        }
    }

    private void openViewer(int startPosition) {
        viewer = new StfalconImageViewer.Builder<>(this, posters,
            Image::setImageAndDecodeBounds)
            .withTransitionFrom(getTransitionTarget(startPosition))
            .withStartPosition(startPosition)
            .withImageChangeListener(position -> {
                if (viewer != null) {
                    currentPosition = position;
                    viewer.updateTransitionImage(getTransitionTarget(position));
                }
            })
            .withDismissListener(() -> isDialogShown = false)
            .show();
        currentPosition = startPosition;
        isDialogShown = true;
    }

    private Image getTransitionTarget(int position) {
        return position == 0 ? rotationDemoImage : null;
    }
}
