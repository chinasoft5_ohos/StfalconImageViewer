/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.adapter;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

import com.stfalcon.sample.ResourceTable;
import com.stfalcon.sample.ability.PostersGridAbility;
import com.stfalcon.sample.ability.RotationAbility;
import com.stfalcon.sample.ability.ScrollingImagesDemoAbility;
import com.stfalcon.sample.ability.StylingAbility;

/**
 * page适配器
 *
 * @since 2021-07-22
 */
public class TabPageAdapter extends PageSliderProvider {
    private static final int INDEX_1 = 0;
    private static final int INDEX_2 = 1;
    private static final int INDEX_3 = 2;
    private static final int MARGIN = 40;
    private final Context context;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public TabPageAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        final int size = 4;
        return size;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        ComponentContainer layout;
        switch (position) {
            case INDEX_1:
                layout = (ComponentContainer) LayoutScatter.getInstance(context).parse(
                    ResourceTable.Layout_layout_images_grid, null, false);
                layout.findComponentById(ResourceTable.Id_demoCardActionButton).setClickedListener(
                    component -> startSubPage(PostersGridAbility.class));
                break;
            case INDEX_2:
                layout = (ComponentContainer) LayoutScatter.getInstance(context).parse(
                    ResourceTable.Layout_layout_scroll, null, false);
                layout.findComponentById(ResourceTable.Id_demoCardActionButton).setClickedListener(
                    component -> startSubPage(ScrollingImagesDemoAbility.class));
                break;
            case INDEX_3:
                layout = (ComponentContainer) LayoutScatter.getInstance(context).parse(
                    ResourceTable.Layout_layout_styling, null, false);
                layout.findComponentById(ResourceTable.Id_demoCardActionButton).setClickedListener(
                    component -> startSubPage(StylingAbility.class));
                break;
            default:
                layout = (ComponentContainer) LayoutScatter.getInstance(context).parse(
                    ResourceTable.Layout_layout_rotation, null, false);
                layout.findComponentById(ResourceTable.Id_demoCardActionButton).setClickedListener(
                    component -> startSubPage(RotationAbility.class));
                break;
        }
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig(
            ComponentContainer.LayoutConfig.MATCH_PARENT,
            ComponentContainer.LayoutConfig.MATCH_PARENT);
        int margin = AttrHelper.vp2px(MARGIN, context);
        layoutConfig.setMargins(margin, 0, margin, 0);
        layout.setLayoutConfig(layoutConfig);
        componentContainer.addComponent(layout);
        return layout;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object object) {
        componentContainer.removeComponent((Component) object);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }

    private void startSubPage(Class<?> ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withBundleName(context.getBundleName())
            .withAbilityName(ability)
            .build();
        intent.setOperation(operation);
        context.startAbility(intent, 0);
    }
}
