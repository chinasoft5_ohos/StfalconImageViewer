/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.dialog;

import ohos.app.Context;

import com.lxj.xpopup.XPopup;

import java.util.TreeMap;

import static com.stfalcon.sample.dialog.StylingOptions.Property.CONTAINER_PADDING;
import static com.stfalcon.sample.dialog.StylingOptions.Property.HIDE_STATUS_BAR;
import static com.stfalcon.sample.dialog.StylingOptions.Property.IMAGES_MARGIN;
import static com.stfalcon.sample.dialog.StylingOptions.Property.RANDOM_BACKGROUND;
import static com.stfalcon.sample.dialog.StylingOptions.Property.SHOW_OVERLAY;
import static com.stfalcon.sample.dialog.StylingOptions.Property.SHOW_TRANSITION;
import static com.stfalcon.sample.dialog.StylingOptions.Property.SWIPE_TO_DISMISS;
import static com.stfalcon.sample.dialog.StylingOptions.Property.ZOOMING;

/**
 * StylingOptions
 *
 * @since 2021-07-26
 */
public class StylingOptions {
    private static StylingOptions options = null;
    private static TreeMap<Property, Boolean> map = new TreeMap<>();

    private StylingOptions() {
        map.put(HIDE_STATUS_BAR, true);
        map.put(IMAGES_MARGIN, true);
        map.put(CONTAINER_PADDING, false);
        map.put(SHOW_TRANSITION, true);
        map.put(SWIPE_TO_DISMISS, true);
        map.put(ZOOMING, true);
        map.put(SHOW_OVERLAY, true);
        map.put(RANDOM_BACKGROUND, false);
    }

    /**
     * 获取单例对象
     *
     * @return StylingOptions
     */
    public static StylingOptions getInstance() {
        if (options == null) {
            options = new StylingOptions();
        }
        return options;
    }

    /**
     * 获取Map集合
     *
     * @return TreeMap
     */
    public TreeMap<Property, Boolean> getMap() {
        return map;
    }

    /**
     * 展示菜单选项弹框
     *
     * @param context 上下文
     */
    public void showDialog(Context context) {
        new XPopup.Builder(context)
            .asCustom(new CustomDialog(context))
            .show();
    }

    /**
     * 获取当前菜单是否勾选
     *
     * @param property key值
     * @return 属性值
     */
    public boolean isPropertyEnabled(Property property) {
        return map.get(property);
    }

    /**
     * Property
     *
     * @since 2021-07-26
     */
    public enum Property {
        /**
         * HIDE_STATUS_BAR
         */
        HIDE_STATUS_BAR,
        /**
         * IMAGES_MARGIN
         */
        IMAGES_MARGIN,
        /**
         * CONTAINER_PADDING
         */
        CONTAINER_PADDING,
        /**
         * SHOW_TRANSITION
         */
        SHOW_TRANSITION,
        /**
         * SWIPE_TO_DISMISS
         */
        SWIPE_TO_DISMISS,
        /**
         * ZOOMING
         */
        ZOOMING,
        /**
         * SHOW_OVERLAY
         */
        SHOW_OVERLAY,
        /**
         * RANDOM_BACKGROUND
         */
        RANDOM_BACKGROUND
    }
}
