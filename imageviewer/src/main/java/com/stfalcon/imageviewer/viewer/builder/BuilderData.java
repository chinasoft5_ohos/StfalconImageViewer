/*
 * Copyright 2018 stfalcon.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.imageviewer.viewer.builder;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;

import com.stfalcon.imageviewer.listeners.OnDismissListener;
import com.stfalcon.imageviewer.listeners.OnImageChangeListener;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.List;

/**
 * 数据定义
 *
 * @since 2021-07-22
 */
public class BuilderData<T> {
    private final List<T> images;
    private final ImageLoader<T> imageLoader;
    private int backgroundColor = Color.BLACK.getValue();
    private int startPosition = 0;
    private OnImageChangeListener imageChangeListener = null;
    private OnDismissListener onDismissListener = null;
    private Component overlayView = null;
    private int imageMarginPixels = 0;
    private int[] containerPaddingPixels = new int[4];
    private boolean shouldStatusBarHide = true;
    private boolean isZoomingAllowed = true;
    private boolean isSwipeToDismissAllowed = true;
    private Image transitionView = null;

    /**
     * 默认构造
     *
     * @param images 图片资源
     * @param imageLoader 图片加载器
     */
    public BuilderData(List<T> images, ImageLoader<T> imageLoader) {
        this.images = images;
        this.imageLoader = imageLoader;
    }

    /**
     * 获取图片
     *
     * @return 图片资源
     */
    public List<T> getImages() {
        return images;
    }

    /**
     * 获取图片加载器
     *
     * @return 图片加载器
     */
    public ImageLoader<T> getImageLoader() {
        return imageLoader;
    }

    /**
     * 获取背景色
     *
     * @return 背景色
     */
    public int getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * 设置背景色
     *
     * @param backgroundColor 背景色
     */
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * 获取开始动画索引
     *
     * @return 开始动画的索引
     */
    public int getStartPosition() {
        return startPosition;
    }

    /**
     * 设置开始动画索引
     *
     * @param startPosition 开始动画的索引
     */
    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    /**
     * 获取图片变更监听器
     *
     * @return 图片变更监听器
     */
    public OnImageChangeListener getImageChangeListener() {
        return imageChangeListener;
    }

    /**
     * 设置图片变更监听器
     *
     * @param imageChangeListener 图片变更监听器
     */
    public void setImageChangeListener(OnImageChangeListener imageChangeListener) {
        this.imageChangeListener = imageChangeListener;
    }

    /**
     * 获取dialog消失监听器
     *
     * @return dialog消失监听器
     */
    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    /**
     * 设置dialog消失监听器
     *
     * @param onDismissListener dialog消失监听器
     */
    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    /**
     * 获取overlay
     *
     * @return overlay组件
     */
    public Component getOverlayView() {
        return overlayView;
    }

    /**
     * 设置overlay
     *
     * @param overlayView overlay组件
     */
    public void setOverlayView(Component overlayView) {
        this.overlayView = overlayView;
    }

    /**
     * 获取image外边距
     *
     * @return image外边距
     */
    public int getImageMarginPixels() {
        return imageMarginPixels;
    }

    /**
     * 设置image外边距
     *
     * @param imageMarginPixels image外边距
     */
    public void setImageMarginPixels(int imageMarginPixels) {
        this.imageMarginPixels = imageMarginPixels;
    }

    /**
     * 获取image内边距
     *
     * @return image内边距
     */
    public int[] getContainerPaddingPixels() {
        return containerPaddingPixels;
    }

    /**
     * 设置内边距
     *
     * @param containerPaddingPixels 内边距
     */
    public void setContainerPaddingPixels(int[] containerPaddingPixels) {
        this.containerPaddingPixels = containerPaddingPixels;
    }

    /**
     * 是否隐藏状态栏
     *
     * @return 是否隐藏状态栏
     */
    public boolean isShouldStatusBarHide() {
        return shouldStatusBarHide;
    }

    /**
     * 设置是否隐藏状态栏
     *
     * @param shouldStatusBarHide 是否隐藏状态栏
     */
    public void setShouldStatusBarHide(boolean shouldStatusBarHide) {
        this.shouldStatusBarHide = shouldStatusBarHide;
    }

    /**
     * 是否可以缩放图片
     *
     * @return 是否可以缩放图片
     */
    public boolean isZoomingAllowed() {
        return isZoomingAllowed;
    }

    /**
     * 设置是否可以缩放图片
     *
     * @param zoomingAllowed 是否可以缩放图片
     */
    public void setZoomingAllowed(boolean zoomingAllowed) {
        isZoomingAllowed = zoomingAllowed;
    }

    /**
     * 是否可以滑动关闭
     *
     * @return 是否可以滑动关闭
     */
    public boolean isSwipeToDismissAllowed() {
        return isSwipeToDismissAllowed;
    }

    /**
     * 设置是否可以滑动关闭
     *
     * @param swipeToDismissAllowed 是否可以滑动关闭
     */
    public void setSwipeToDismissAllowed(boolean swipeToDismissAllowed) {
        isSwipeToDismissAllowed = swipeToDismissAllowed;
    }

    /**
     * 获取当前转场的image
     *
     * @return 当前转场的image
     */
    public Image getTransitionView() {
        return transitionView;
    }

    /**
     * 设置当前转场的image
     *
     * @param transitionView 当前转场的image
     */
    public void setTransitionView(Image transitionView) {
        this.transitionView = transitionView;
    }
}
