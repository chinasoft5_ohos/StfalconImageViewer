/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.ability;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;

import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.sample.ResourceTable;

import java.util.ArrayList;
import java.util.List;

/**
 * PostersGridAbility
 *
 * @since 2021-07-22
 */
public class PostersGridAbility extends Ability {
    private static final int POS_0 = 0;
    private static final int POS_1 = 1;
    private static final int POS_2 = 2;
    private static final int POS_3 = 3;
    private static final int POS_4 = 4;
    private static final int POS_5 = 5;
    private static final int POS_6 = 6;
    private static final int POS_7 = 7;
    private static final int POS_8 = 8;
    private StfalconImageViewer<Integer> imageViewer;
    private final List<Image> horizontalImages = new ArrayList<>();
    private final List<Integer> horizontalResources = new ArrayList<>();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_grid);
        initViews();
        initImage();
    }

    private void initImage() {
        horizontalResources.add(ResourceTable.Media_vincent);
        horizontalResources.add(ResourceTable.Media_jules);
        horizontalResources.add(ResourceTable.Media_korben);
        horizontalResources.add(ResourceTable.Media_toretto);
        horizontalResources.add(ResourceTable.Media_marty);
        horizontalResources.add(ResourceTable.Media_driver);
        horizontalResources.add(ResourceTable.Media_frank);
        horizontalResources.add(ResourceTable.Media_max);
        horizontalResources.add(ResourceTable.Media_daniel);
        for (Image image : horizontalImages) {
            image.setClickedListener(component -> {
                int startPosition;
                switch (component.getId()) {
                    case ResourceTable.Id_postersSecondImage:
                        startPosition = POS_1;
                        break;
                    case ResourceTable.Id_postersThirdImage:
                        startPosition = POS_2;
                        break;
                    case ResourceTable.Id_postersFourthImage:
                        startPosition = POS_3;
                        break;
                    case ResourceTable.Id_postersFifthImage:
                        startPosition = POS_4;
                        break;
                    case ResourceTable.Id_postersSixthImage:
                        startPosition = POS_5;
                        break;
                    case ResourceTable.Id_postersSeventhImage:
                        startPosition = POS_6;
                        break;
                    case ResourceTable.Id_postersEighthImage:
                        startPosition = POS_7;
                        break;
                    case ResourceTable.Id_postersNinthImage:
                        startPosition = POS_8;
                        break;
                    default:
                        startPosition = POS_0;
                        break;
                }
                openViewer(image, startPosition);
            });
        }
    }

    private void openViewer(Image image, int startPosition) {
        imageViewer = new StfalconImageViewer.Builder<>(getContext(), horizontalResources,
            Image::setImageAndDecodeBounds)
            .withStartPosition(startPosition)
            .withImageChangeListener(position -> {
                if (imageViewer != null) {
                    imageViewer.updateTransitionImage(horizontalImages.get(position));
                }
            })
            .withTransitionFrom(image).show();
    }

    private void initViews() {
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersFirstImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersSecondImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersThirdImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersFourthImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersFifthImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersSixthImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersSeventhImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersEighthImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_postersNinthImage));
    }
}
