/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.dialog;

import ohos.agp.components.AbsButton;
import ohos.agp.components.Checkbox;
import ohos.app.Context;

import com.lxj.xpopup.core.CenterPopupView;
import com.stfalcon.sample.ResourceTable;

/**
 * 自定义弹框
 *
 * @since 2021-07-26
 */
public class CustomDialog extends CenterPopupView implements AbsButton.CheckedStateChangedListener {
    Checkbox cbHide = null;
    Checkbox cbMargin = null;
    Checkbox cbPadding = null;
    Checkbox cbTransitions = null;
    Checkbox cbSwipe = null;
    Checkbox cbZoom = null;
    Checkbox cbOverlay = null;
    Checkbox cbRandom = null;
    StylingOptions options = null;

    /**
     * CustomDialog
     *
     * @param context context
     */
    public CustomDialog(Context context) {
        super(context, null);
    }

    @Override
    protected int getImplLayoutId() {
        return ResourceTable.Layout_layout_dialog_styled;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        cbHide = (Checkbox) findComponentById(ResourceTable.Id_cb_hide);
        cbMargin = (Checkbox) findComponentById(ResourceTable.Id_cb_margin);
        cbPadding = (Checkbox) findComponentById(ResourceTable.Id_cb_padding);
        cbTransitions = (Checkbox) findComponentById(ResourceTable.Id_cb_transitions);
        cbSwipe = (Checkbox) findComponentById(ResourceTable.Id_cb_swipe);
        cbZoom = (Checkbox) findComponentById(ResourceTable.Id_cb_zoom);
        cbOverlay = (Checkbox) findComponentById(ResourceTable.Id_cb_overlay);
        cbRandom = (Checkbox) findComponentById(ResourceTable.Id_cb_radnom);
        options = StylingOptions.getInstance();
        cbHide.setChecked(options.isPropertyEnabled(StylingOptions.Property.HIDE_STATUS_BAR));
        cbMargin.setChecked(options.isPropertyEnabled(StylingOptions.Property.IMAGES_MARGIN));
        cbPadding.setChecked(options.isPropertyEnabled(StylingOptions.Property.CONTAINER_PADDING));
        cbTransitions.setChecked(options.isPropertyEnabled(StylingOptions.Property.SHOW_TRANSITION));
        cbSwipe.setChecked(options.isPropertyEnabled(StylingOptions.Property.SWIPE_TO_DISMISS));
        cbZoom.setChecked(options.isPropertyEnabled(StylingOptions.Property.ZOOMING));
        cbOverlay.setChecked(options.isPropertyEnabled(StylingOptions.Property.SHOW_OVERLAY));
        cbRandom.setChecked(options.isPropertyEnabled(StylingOptions.Property.RANDOM_BACKGROUND));
        cbHide.setCheckedStateChangedListener(this);
        cbMargin.setCheckedStateChangedListener(this);
        cbPadding.setCheckedStateChangedListener(this);
        cbTransitions.setCheckedStateChangedListener(this);
        cbSwipe.setCheckedStateChangedListener(this);
        cbZoom.setCheckedStateChangedListener(this);
        cbOverlay.setCheckedStateChangedListener(this);
        cbRandom.setCheckedStateChangedListener(this);
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
        switch (absButton.getId()) {
            case ResourceTable.Id_cb_hide:
                options.getMap().put(StylingOptions.Property.HIDE_STATUS_BAR, isChecked);
                break;
            case ResourceTable.Id_cb_margin:
                options.getMap().put(StylingOptions.Property.IMAGES_MARGIN, isChecked);
                break;
            case ResourceTable.Id_cb_padding:
                options.getMap().put(StylingOptions.Property.CONTAINER_PADDING, isChecked);
                break;
            case ResourceTable.Id_cb_transitions:
                options.getMap().put(StylingOptions.Property.SHOW_TRANSITION, isChecked);
                break;
            case ResourceTable.Id_cb_swipe:
                options.getMap().put(StylingOptions.Property.SWIPE_TO_DISMISS, isChecked);
                break;
            case ResourceTable.Id_cb_zoom:
                options.getMap().put(StylingOptions.Property.ZOOMING, isChecked);
                break;
            case ResourceTable.Id_cb_overlay:
                options.getMap().put(StylingOptions.Property.SHOW_OVERLAY, isChecked);
                break;
            case ResourceTable.Id_cb_radnom:
                options.getMap().put(StylingOptions.Property.RANDOM_BACKGROUND, isChecked);
                break;
            default:
                break;
        }
    }
}
