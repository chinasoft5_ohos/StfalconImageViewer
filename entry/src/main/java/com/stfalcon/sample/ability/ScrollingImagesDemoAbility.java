/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.ability;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;

import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.sample.ResourceTable;

import java.util.ArrayList;
import java.util.List;

/**
 * 滑动页面
 *
 * @since 2021-07-23
 */
public class ScrollingImagesDemoAbility extends Ability {
    private static final int POS_0 = 0;
    private static final int POS_1 = 1;
    private static final int POS_2 = 2;
    private static final int POS_3 = 3;
    private final List<Image> horizontalImages = new ArrayList<>();
    private final List<Integer> horizontalResources = new ArrayList<>();
    private final List<Image> verticalImages = new ArrayList<>();
    private final List<Integer> verticalResources = new ArrayList<>();
    private StfalconImageViewer<Integer> imageViewer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_scrolling_images_demo);
        initHorizontalImages();
        initVerticalImages();
    }

    private void initHorizontalImages() {
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_scrollingHorizontalFirstImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_scrollingHorizontalSecondImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_scrollingHorizontalThirdImage));
        horizontalImages.add((Image) findComponentById(ResourceTable.Id_scrollingHorizontalFourthImage));
        horizontalResources.add(ResourceTable.Media_horizontal_colorful_1);
        horizontalResources.add(ResourceTable.Media_square_colorful);
        horizontalResources.add(ResourceTable.Media_horizontal_colorful_2);
        horizontalResources.add(ResourceTable.Media_horizontal_colorful_3);
        for (Image image : horizontalImages) {
            image.setClickedListener(component -> {
                int startPosition;
                switch (component.getId()) {
                    case ResourceTable.Id_scrollingHorizontalSecondImage:
                        startPosition = POS_1;
                        break;
                    case ResourceTable.Id_scrollingHorizontalThirdImage:
                        startPosition = POS_2;
                        break;
                    case ResourceTable.Id_scrollingHorizontalFourthImage:
                        startPosition = POS_3;
                        break;
                    default:
                        startPosition = POS_0;
                        break;
                }
                imageViewer = new StfalconImageViewer.Builder<>(getContext(), horizontalResources,
                    Image::setImageAndDecodeBounds)
                    .withStartPosition(startPosition)
                    .withImageChangeListener(position -> {
                        if (imageViewer != null) {
                            imageViewer.updateTransitionImage(horizontalImages.get(position));
                        }
                    })
                    .withTransitionFrom(image).show();
            });
        }
    }

    private void initVerticalImages() {
        verticalImages.add((Image) findComponentById(ResourceTable.Id_scrollingVerticalFirstImage));
        verticalImages.add((Image) findComponentById(ResourceTable.Id_scrollingVerticalSecondImage));
        verticalImages.add((Image) findComponentById(ResourceTable.Id_scrollingVerticalThirdImage));
        verticalImages.add((Image) findComponentById(ResourceTable.Id_scrollingVerticalFourthImage));
        verticalResources.add(ResourceTable.Media_vertical_colorful_1);
        verticalResources.add(ResourceTable.Media_vertical_colorful_2);
        verticalResources.add(ResourceTable.Media_vertical_colorful_3);
        verticalResources.add(ResourceTable.Media_vertical_colorful_4);
        for (Image image : verticalImages) {
            image.setClickedListener(component -> {
                int startPosition;
                switch (component.getId()) {
                    case ResourceTable.Id_scrollingVerticalSecondImage:
                        startPosition = POS_1;
                        break;
                    case ResourceTable.Id_scrollingVerticalThirdImage:
                        startPosition = POS_2;
                        break;
                    case ResourceTable.Id_scrollingVerticalFourthImage:
                        startPosition = POS_3;
                        break;
                    default:
                        startPosition = POS_0;
                        break;
                }
                imageViewer = new StfalconImageViewer.Builder<>(getContext(), verticalResources,
                    Image::setImageAndDecodeBounds)
                    .withStartPosition(startPosition)
                    .withImageChangeListener(position -> {
                        if (imageViewer != null) {
                            imageViewer.updateTransitionImage(verticalImages.get(position));
                        }
                    })
                    .withTransitionFrom(image).show();
            });
        }
    }
}
