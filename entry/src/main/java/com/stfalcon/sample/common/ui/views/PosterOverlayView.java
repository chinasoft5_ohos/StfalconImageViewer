/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.common.ui.views;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import com.stfalcon.sample.ResourceTable;
import com.stfalcon.sample.common.models.Poster;

/**
 * PosterOverlayView
 *
 * @since 2021-07-27
 */
public class PosterOverlayView extends StackLayout {
    private Text text;
    private Image delete;
    private Image share;

    /**
     * PosterOverlayView
     *
     * @param context 上下文
     */
    public PosterOverlayView(Context context) {
        this(context, null);
    }

    /**
     * PosterOverlayView
     *
     * @param context 上下文
     * @param attrSet 自定义属性
     */
    public PosterOverlayView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * PosterOverlayView
     *
     * @param context 上下文
     * @param attrSet 自定义属性
     * @param styleName styleName
     */
    public PosterOverlayView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        Component rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_overlay,
            this, true);
        text = (Text) rootView.findComponentById(ResourceTable.Id_content);
        delete = (Image) rootView.findComponentById(ResourceTable.Id_posterOverlayDeleteButton);
        share = (Image) rootView.findComponentById(ResourceTable.Id_posterOverlayShareButton);
    }

    /**
     * 更新当前显示UI
     *
     * @param newPoster 新的数据
     */
    public void update(Poster newPoster) {
        text.setText(newPoster.getDescription());
        share.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intents = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withUri(Uri.parse(newPoster.getUrl()))
                    .withAction(IntentConstants.ACTION_SEARCH)
                    .build();
                intents.setOperation(operation);
                getContext().startAbility(intents, 0);
            }
        });
    }
}
