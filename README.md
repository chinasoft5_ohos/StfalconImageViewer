# StfalconImageViewer

#### 项目介绍
- 项目名称：StfalconImageViewer
- 所属系列：openharmony的第三方组件适配移植
- 功能：全屏图片查看器
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示
<img src="img/sample.gif"/>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:stfalconimageviewer:1.0.0')
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
通过`StfalconImageViewer.Builder`构造器设置各个属性，并构建`StfalconImageViewer`实例进行展示。
```java
        new StfalconImageViewer.Builder<Poster>(this, posters,
            new ImageLoader<Poster>() {
                @Override
                public void loadImage(Image imageView, Poster image) {
                    imageView.setImageAndDecodeBounds(image);
                }
            })
            .withTransitionFrom(getTransitionTarget(startPosition))
            .withStartPosition(startPosition)
            .withBackgroundColor(color)
            .withOverlayView(view)
            .withImagesMargin(56)
            .withContainerPadding(56)
            .withHiddenStatusBar(shouldHideStatusBar)
            .allowZooming(isZoomingAllowed)
            .allowSwipeToDismiss(isSwipeToDismissAllowed)
            .withImageChangeListener(position -> {
                if (viewer != null) {
                    viewer.updateTransitionImage(getTransitionTarget(position));
                }
            })
            .withDismissListener(() -> {})
            .show();
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Copyright (C) 2018 stfalcon.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
