/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.sample.common.models;

/**
 * 海报
 *
 * @since 2021-07-16
 */
public class Poster {
    private final String url;
    private final String description;

    /**
     * 默认构造
     *
     * @param url 图片地址
     * @param description 图片描述
     */
    public Poster(String url, String description) {
        this.url = url;
        this.description = description;
    }

    /**
     * 获取图片地址
     *
     * @return 图片地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 获取图片描述
     *
     * @return 图片描述
     */
    public String getDescription() {
        return description;
    }
}
